package co.brandonr.fullsail.adp2recipeproject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by brandon on 11/10/16.
 */

public class Recipe implements Serializable {
    private final String title;
    private final String imageUrl;
    private final int recipeId;
    private final int usedIngredients;
    private final int missingIngredients;
    private final int likes;
    private final ArrayList<Ingredient> missingArray;
    private final String missingString;

    public Recipe(String title, String imageUrl, int recipeId, int usedIngredients, int missingIngredients, int likes, ArrayList<Ingredient> missingArray, String missingString) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.recipeId = recipeId;
        this.usedIngredients = usedIngredients;
        this.missingIngredients = missingIngredients;
        this.likes = likes;
        this.missingArray = missingArray;
        this.missingString = missingString;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public int getUsedIngredients() {
        return usedIngredients;
    }

    public int getMissingIngredients() {
        return missingIngredients;
    }

    public int getLikes() {
        return likes;
    }

    public ArrayList<Ingredient> getMissingArray() {
        return missingArray;
    }

    public String getMissingString() {
        return missingString;
    }
}
