package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;

import java.util.ArrayList;

import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.Ingredient;
import co.brandonr.fullsail.adp2recipeproject.StorageUtils;

/**
 * Created by brandon on 11/2/16.
 */

public class AddIngredientFragment extends ListFragment {
    public static AddIngredientFragment newInstance() {
        return new AddIngredientFragment();
    }

    private final ArrayList<String> sampleIngredients = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sampleIngredients.add("Chicken");
        sampleIngredients.add("Potato");
        sampleIngredients.add("Tomato");
        sampleIngredients.add("Carrot");
        sampleIngredients.add("Rice");
        sampleIngredients.add("Pickle");
        sampleIngredients.add("Turkey");
        sampleIngredients.add("Sweet Potato");
        sampleIngredients.add("Corn");
        sampleIngredients.add("Bread");
        sampleIngredients.add("Onion");
        sampleIngredients.add("Garlic");
        sampleIngredients.add("Pasta");
        updateList();
    }

    @Override
    public void onListItemClick(ListView l, View v, final int position, long id) {
        super.onListItemClick(l, v, position, id);
        final NumberPicker picker = Globals.generatePicker(getActivity());
        new AlertDialog.Builder(getActivity())
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        writeIngredient(position, picker.getValue());
                    }
                })
                .setTitle(sampleIngredients.get(position))
                .setMessage("How many would you like to add?")
                .setView(picker)
                .show();
    }

    private void writeIngredient(int position, int pickerValue) {
        Ingredient ingredient = new Ingredient(sampleIngredients.get(position), pickerValue);
        for (Ingredient storedIngredient : StorageUtils.readIngredients(getContext(), Globals.ADD_EXTRA)) {
            if (storedIngredient.getmName().equals(ingredient.getmName()) && ingredient.getmQuantity() != 0) {
                StorageUtils.updateIngredient(getActivity(), ingredient, (storedIngredient.getmQuantity() + ingredient.getmQuantity()), Globals.ADD_EXTRA);
                return;
            }
        }
        if (ingredient.getmQuantity() != 0) {
            StorageUtils.writeIngredient(getActivity(), ingredient,
                    getActivity().getIntent().getStringExtra(Globals.ADD_EXTRA));
        }
    }

    private void updateList() {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,
                        sampleIngredients);
        setListAdapter(adapter);
    }

    public void filterList(String filter) {
        ((ArrayAdapter<String>) getListAdapter()).getFilter().filter(filter);
    }
}

