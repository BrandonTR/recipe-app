package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import java.net.URLEncoder;

import co.brandonr.fullsail.adp2recipeproject.GetRecipesTask;
import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.Ingredient;
import co.brandonr.fullsail.adp2recipeproject.Recipe;
import co.brandonr.fullsail.adp2recipeproject.RecipeAdapter;
import co.brandonr.fullsail.adp2recipeproject.StorageUtils;
import co.brandonr.fullsail.adp2recipeproject.activities.RecipeActivity;

/**
 * Created by brandon on 11/10/16.
 */

public class RecipeSearchFragment extends ListFragment {
    public static RecipeSearchFragment newInstance() {
        return new RecipeSearchFragment();
    }

    private int filter = 2;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchAllIngredients();
        getListView().setDivider(null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(getActivity(), RecipeActivity.class);
        intent.putExtra(Globals.RECIPE_EXTRA, (Recipe) getListAdapter().getItem(position));
        getActivity().startActivity(intent);
    }

    private void searchAllIngredients() {
        String query = "";
        for (Ingredient ingredient : StorageUtils.readIngredients(getActivity(), Globals.INGREDIENTS_DATA)) {
            try {
                query = query.concat(URLEncoder.encode(ingredient.getmName() + ",", "UTF-8"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        checkAndExecute(Globals.getSearchRecipeUrl(filter, query));
    }

    private void checkAndExecute(String query) {
        GetRecipesTask getRecipesTask = new GetRecipesTask(getActivity());

        if (Globals.connected(getActivity())) {
            getRecipesTask.execute(query);
        } else {
            Globals.noInternetToast(getActivity());
        }
    }

    public void setFilter(int filter) {
        this.filter = filter;
        searchAllIngredients();
    }

    public void filterList(String filter) {
        ((RecipeAdapter) getListAdapter()).getFilter().filter(filter);
    }
}
