package co.brandonr.fullsail.adp2recipeproject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by brandon on 11/10/16.
 */

public class ActualRecipe implements Serializable {

    private final String imageUrl;
    private final String ingredientString;
    private final ArrayList<Ingredient> ingredientArray;
    private final String publisher;
    private final String sourceUrl;
    private final String title;
    private final String instructions;

    public ActualRecipe(String imageUrl, String ingredientString, ArrayList<Ingredient> ingredientArray, String publisher, String sourceUrl, String title, String instructions) {
        this.imageUrl = imageUrl;
        this.ingredientString = ingredientString;
        this.ingredientArray = ingredientArray;
        this.publisher = publisher;
        this.sourceUrl = sourceUrl;
        this.title = title;
        this.instructions = instructions;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getIngredientString() {
        return ingredientString;
    }

    public ArrayList<Ingredient> getIngredientArray() {
        // Saved for a future implementation. (Used Ingredients)
        return ingredientArray;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getInstructions() {
        return instructions;
    }
}
