package co.brandonr.fullsail.adp2recipeproject.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.fragments.AccountFragment;
import co.brandonr.fullsail.adp2recipeproject.fragments.LoginFragment;
import co.brandonr.fullsail.adp2recipeproject.fragments.ResetPasswordFragment;
import co.brandonr.fullsail.adp2recipeproject.fragments.SignUpFragment;

public class AccountActivity extends AppCompatActivity implements
        LoginFragment.AccountActionListener, SignUpFragment.AccountActionListener,
        ResetPasswordFragment.AccountActionListener {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        getSupportActionBar().setElevation(0);
        setTitle("Account");

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.account_frame,
                            AccountFragment.newInstance()).commit();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.account_frame,
                            LoginFragment.newInstance()).commit();
                }
            }
        };


    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT);
    }

    public void forgotPassword(final String email) {
        if (email.equals("")) {
            new AlertDialog.Builder(this).setNeutralButton("Close", null)
                    .setTitle("Invalid Credentials").setMessage("Please enter the e-mail " +
                    "that you would like to reset the password for.").show();
        } else {
            new AlertDialog.Builder(this).setNeutralButton("No", null)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mAuth.sendPasswordResetEmail(email);
                            showToast("Password reset email sent!");
                        }
                    })
                    .setTitle("Reset Password?").setMessage("Would you like to " +
                    "reset the password for the e-mail provided? (An e-mail will be sent " +
                    "with further instruction.)").show();
        }
    }

    public void signUp(String email, String password) {
        //TODO Implement telling the user why sign up failed.
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            showToast("Sign up failed.");

                        } else {
                            showToast("Signed up!");
                        }
                    }
                });
    }

    public void signIn(final String email, final String password, final View view) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            Snackbar.make(view, "Authentication Failed.", Snackbar.LENGTH_SHORT)
                                    .setAction("Sign Up?", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            signUp(email, password);
                                        }
                                    }).show();

                        } else {
                            showToast("Signed in!");
                        }
                    }
                });
    }

    public void showSignUp() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.account_frame, SignUpFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    public void showForgotPassword() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.account_frame, ResetPasswordFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }
}
