package co.brandonr.fullsail.adp2recipeproject.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;

import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.fragments.AddIngredientFragment;

public class IngredientAddActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private AddIngredientFragment addIngredientFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setTitle("Search");

        addIngredientFragment = AddIngredientFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.search_frame,
                addIngredientFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_ingredient, menu);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        addIngredientFragment.filterList(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        addIngredientFragment.filterList(newText);
        return false;
    }
}
