package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.ArrayList;

import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.Ingredient;
import co.brandonr.fullsail.adp2recipeproject.IngredientAdapter;
import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.StorageUtils;

/**
 * Created by brandon on 11/1/16.
 */

public class PantryFragment extends ListFragment {
    public static PantryFragment newInstance() {
        return new PantryFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        updateList();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView emptyText = (TextView) getActivity().findViewById(R.id.empty_textView);
        getListView().setEmptyView(emptyText);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        ArrayList<Ingredient> ingredients = StorageUtils.readIngredients(getActivity(), Globals.INGREDIENTS_DATA);
        final Ingredient ingredient = ingredients.get(position);
        final NumberPicker picker = Globals.generatePicker(getActivity());
        new AlertDialog.Builder(getActivity())
                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (picker.getValue() == 0) {
                            StorageUtils.deleteIngredient(getActivity(), ingredient, Globals.INGREDIENTS_DATA);
                        } else {
                            StorageUtils.updateIngredient(getActivity(), ingredient, picker.getValue(), Globals.INGREDIENTS_DATA);
                        }
                        updateList();
                    }
                })
                .setNegativeButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        StorageUtils.deleteIngredient(getActivity(), ingredient, Globals.INGREDIENTS_DATA);
                        updateList();
                    }
                })
                .setNeutralButton("Cancel", null)
                .setTitle(ingredient.getmName())
                .setMessage("Edit Ingredient")
                .setView(picker)
                .show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_pantry, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Globals.launchOptionsAction(item.getItemId(), getActivity(), Globals.INGREDIENTS_DATA);
        return super.onOptionsItemSelected(item);
    }

    private void updateList() {

        ArrayList<Ingredient> ingredients = StorageUtils.readIngredients(getActivity(), Globals.INGREDIENTS_DATA);
        IngredientAdapter adapter = new IngredientAdapter(ingredients, getActivity());
        setListAdapter(adapter);
    }
}
