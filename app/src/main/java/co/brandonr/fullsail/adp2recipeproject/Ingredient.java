package co.brandonr.fullsail.adp2recipeproject;

import java.io.Serializable;

/**
 * Created by brandon on 11/2/16.
 */

public class Ingredient implements Serializable {
    private final String mName;
    private final double mQuantity;

    public Ingredient(String mName, double mQuantity) {
        this.mName = mName;
        this.mQuantity = mQuantity;
    }

    public String getmName() {
        return mName;
    }

    public double getmQuantity() {
        return mQuantity;
    }

}
