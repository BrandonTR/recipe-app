package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import co.brandonr.fullsail.adp2recipeproject.ActualRecipe;
import co.brandonr.fullsail.adp2recipeproject.FavoriteAdapter;
import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.StorageUtils;
import co.brandonr.fullsail.adp2recipeproject.activities.FavoriteActivity;

/**
 * Created by brandon on 11/1/16.
 */

public class FavoritesFragment extends ListFragment {
    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        updateList();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateList();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setDivider(null);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView emptyText = (TextView) getActivity().findViewById(R.id.empty_textView);
        getListView().setEmptyView(emptyText);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_favorites, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Globals.launchOptionsAction(item.getItemId(), getActivity(), "no.obj");
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(getActivity(), FavoriteActivity.class);
        intent.putExtra(Globals.FAVORITE_EXTRA, (ActualRecipe) getListAdapter().getItem(position));
        getActivity().startActivity(intent);
    }

    private void updateList() {
        ArrayList<ActualRecipe> favorites = StorageUtils.readFavorites(getActivity());
        FavoriteAdapter adapter = new FavoriteAdapter(favorites, getActivity());
        setListAdapter(adapter);
    }
}
