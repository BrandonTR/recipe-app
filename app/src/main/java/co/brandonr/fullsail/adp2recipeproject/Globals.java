package co.brandonr.fullsail.adp2recipeproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import co.brandonr.fullsail.adp2recipeproject.activities.AccountActivity;
import co.brandonr.fullsail.adp2recipeproject.activities.IngredientAddActivity;
import co.brandonr.fullsail.adp2recipeproject.activities.RecipeSearchActivity;

/**
 * Created by brandon on 11/2/16.
 */

public class Globals {
    public static final String INGREDIENTS_DATA = "ingredients.obj";
    public static final String SHOPPING_DATA = "shopping.obj";
    public static final String FAVORITE_DATA = "favorites.obj";

    public static final String RECIPE_EXTRA = "co.brandonr.fullsail.recipe_extra";
    public static final String FAVORITE_EXTRA = "co.brandonr.fullsail.favorite_extra";
    public static final String ADD_EXTRA = "co.brandonr.fullsail.add_extra";


    public static void launchOptionsAction(int id, Activity activity, String extra) {
        switch (id) {
            case R.id.action_login:
                activity.startActivity(new Intent(activity, AccountActivity.class));
                break;
            case R.id.action_add_ingredient:
                Intent intent = new Intent(activity, IngredientAddActivity.class);
                intent.putExtra(ADD_EXTRA, extra);
                activity.startActivity(intent);
                break;
            case R.id.action_search:
                activity.startActivity(new Intent(activity, RecipeSearchActivity.class));
                break;
        }
    }

    public static String getSearchRecipeUrl(int filter, String ingredients) {
        try {
            return "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/" +
                    "findByIngredients?fillIngredients=true&limitLicense=false&number=30&ranking="
                    + URLEncoder.encode(String.valueOf(filter), "UTF-8") + "&ingredients="
                    + ingredients;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getRecipeUrl(int id) {
        try {
            return "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/" +
                    URLEncoder.encode(String.valueOf(id), "UTF-8") +
                    "/information?includeNutrition=false";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void accountInfoAlert(Activity activity) {
        new AlertDialog.Builder(activity).setNeutralButton("Close", null)
                .setTitle("Account Benefits")
                .setMessage("With a Recipe App account you open yourself to another level of " +
                        "cooking. When you create an account new features will be available to you" +
                        " such as pantry syncing, favorites syncing, and shopping list syncing! " +
                        "down the road more features may be added to benefit users with an account.")
                .show();
    }

    public static boolean genericFieldCheck(String email, String password, Activity activity) {
        if (email.equals("") || password.equals("")) {
            Toast.makeText(activity, "Please enter valid credentials.", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public static NumberPicker generatePicker(Activity activity) {
        final NumberPicker picker = new NumberPicker(activity);
        picker.setMinValue(0);
        picker.setMaxValue(100);
        return picker;
    }

    public static void noInternetToast(Activity activity) {
        Toast.makeText(activity, "No Internet Connection.", Toast.LENGTH_SHORT);
    }

    public static boolean connected(Activity activity) {
        ConnectivityManager mgr = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (mgr != null) {
            NetworkInfo info = mgr.getActiveNetworkInfo();

            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            } else {
                return false;
            }

        }
        return false;
    }
}
