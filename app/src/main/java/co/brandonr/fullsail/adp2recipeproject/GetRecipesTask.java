package co.brandonr.fullsail.adp2recipeproject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by brandon on 4/26/16.
 */
public class GetRecipesTask extends AsyncTask<String, Void, String> {

    public interface TaskCompletedListener {
        void setListAdapter(RecipeAdapter adapter);
    }

    private TaskCompletedListener mListener;
    private ProgressDialog progressDialog;
    private final Context context;

    public GetRecipesTask(Context context) {
        this.context = context;
        if (context instanceof TaskCompletedListener) {
            mListener = (TaskCompletedListener) context;
        } else {
            throw new IllegalArgumentException("TaskCompleted" +
                    "Listener interface not implemented.");
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Working");
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            URL url = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("X-Mashape-Key", "5eWoDTc8EImsh64IdaqPAoDhF6P8p1Y90LijsnEQwWtPjNA5db");
            connection.setRequestProperty("Accept", "application/json");
            try {
                connection.connect();

                InputStream is = connection.getInputStream();

                String data = IOUtils.toString(is);

                is.close();

                return data;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // Close connection
                connection.disconnect();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Error";
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        progressDialog.cancel();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        JSONArray recipesArray;

        try {
            recipesArray = new JSONArray(s);
            RecipeAdapter recipeAdapter = new RecipeAdapter(context, recipesArray);
            mListener.setListAdapter(recipeAdapter);
            progressDialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}