package co.brandonr.fullsail.adp2recipeproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by brandon on 11/17/16.
 */

public class FavoriteAdapter extends BaseAdapter {
    private final ArrayList<ActualRecipe> mFavorites;
    private final Context mContext;

    public FavoriteAdapter(ArrayList<ActualRecipe> mFavorites, Context mContext) {
        this.mFavorites = mFavorites;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mFavorites.size();
    }

    @Override
    public ActualRecipe getItem(int i) {
        return mFavorites.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ActualRecipe recipe = mFavorites.get(i);

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.cell_favorite,
                    viewGroup, false);
        }

        Picasso.with(mContext).load(recipe.getImageUrl()).into(
                ((ImageView) view.findViewById(R.id.favorite_image)), null);
        ((TextView) view.findViewById(R.id.favorite_publisher)).setText(recipe.getPublisher());
        ((TextView) view.findViewById(R.id.favorite_title)).setText(recipe.getTitle());

        return view;
    }
}
