package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.R;

/**
 * Created by brandon on 11/8/16.
 */

public class SignUpFragment extends Fragment implements View.OnClickListener {
    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    private EditText emailInput;
    private EditText passwordInput;

    public interface AccountActionListener {
        void signUp(String email, String password);
    }

    private SignUpFragment.AccountActionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignUpFragment.AccountActionListener) {
            mListener = (SignUpFragment.AccountActionListener) context;
        } else {
            throw new IllegalArgumentException("AccountAction" +
                    "Listener interface not implemented.");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_signup, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailInput = ((EditText) view.findViewById(R.id.input_username1));
        passwordInput = ((EditText) view.findViewById(R.id.input_password1));
        view.findViewById(R.id.button_signUp1).setOnClickListener(this);
        view.findViewById(R.id.input_password1).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    mListener.signUp(emailInput.getText().toString(), passwordInput.getText().toString());
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (Globals.genericFieldCheck(emailInput.getText().toString(), passwordInput.getText().toString(), getActivity())) {
            return;
        }
        switch (view.getId()) {
            case R.id.button_signUp1:
                mListener.signUp(emailInput.getText().toString(), passwordInput.getText().toString());
                break;
        }
    }
}
