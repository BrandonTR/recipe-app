package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import co.brandonr.fullsail.adp2recipeproject.R;

/**
 * Created by brandon on 11/8/16.
 */

public class ResetPasswordFragment extends Fragment implements View.OnClickListener {
    public static ResetPasswordFragment newInstance() {
        return new ResetPasswordFragment();
    }

    private EditText emailInput;

    public interface AccountActionListener {
        void forgotPassword(String email);
    }

    private ResetPasswordFragment.AccountActionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ResetPasswordFragment.AccountActionListener) {
            mListener = (ResetPasswordFragment.AccountActionListener) context;
        } else {
            throw new IllegalArgumentException("AccountAction" +
                    "Listener interface not implemented.");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_resetpassword, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailInput = ((EditText) view.findViewById(R.id.input_username2));
        view.findViewById(R.id.button_forgotPassword2).setOnClickListener(this);
        view.findViewById(R.id.input_username2).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    mListener.forgotPassword(emailInput.getText().toString());
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (emailInput.getText().toString().equals("") || emailInput.getText() == null) {
            Toast.makeText(getActivity(), "Please enter valid credentials.", Toast.LENGTH_SHORT).show();
            return;
        }
        switch (view.getId()) {
            case R.id.button_forgotPassword2:
                mListener.forgotPassword(emailInput.getText().toString());
                break;
        }
    }
}
