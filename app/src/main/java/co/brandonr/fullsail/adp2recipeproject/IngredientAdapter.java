package co.brandonr.fullsail.adp2recipeproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by brandon on 11/2/16.
 */

public class IngredientAdapter extends BaseAdapter {
    private final ArrayList<Ingredient> mIngredients;
    private final Context mContext;

    public IngredientAdapter(ArrayList<Ingredient> mIngredients, Context mContext) {
        this.mIngredients = mIngredients;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mIngredients.size();
    }

    @Override
    public Ingredient getItem(int i) {
        return mIngredients.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Ingredient ingredient = mIngredients.get(i);

        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.cell_ingredient,
                    viewGroup, false);
        }

        ((TextView) view.findViewById(R.id.ingredient_name)).setText(ingredient.getmName());
        ((TextView) view.findViewById(R.id.ingredient_quantity)).setText("" + ingredient.getmQuantity());


        return view;
    }
}
