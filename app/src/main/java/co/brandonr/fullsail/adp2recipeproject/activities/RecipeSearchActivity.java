package co.brandonr.fullsail.adp2recipeproject.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import co.brandonr.fullsail.adp2recipeproject.GetRecipesTask;
import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.RecipeAdapter;
import co.brandonr.fullsail.adp2recipeproject.fragments.RecipeSearchFragment;

public class RecipeSearchActivity extends AppCompatActivity implements GetRecipesTask.TaskCompletedListener, SearchView.OnQueryTextListener {

    private RecipeSearchFragment searchFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_search);
        searchFragment = RecipeSearchFragment.newInstance();

        getSupportFragmentManager().beginTransaction().replace(R.id.activity_recipe_search,
                searchFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final RadioGroup group = new RadioGroup(this);
        final RadioButton filterOne = new RadioButton(this);
        RadioButton filterTwo = new RadioButton(this);
        filterOne.setText("Maximize Used Ingredients");
        filterTwo.setText("Minimize Missing Ingredients");
        group.addView(filterOne);
        group.addView(filterTwo);
        switch (item.getItemId()) {
            case R.id.filter:
                new AlertDialog.Builder(this)
                        .setNeutralButton("Cancel", null)
                        .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (filterOne.isChecked()) {
                                    searchFragment.setFilter(1);
                                } else {
                                    searchFragment.setFilter(2);
                                }
                            }
                        })
                        .setTitle("Filter")
                        .setView(group)
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchFragment.filterList(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchFragment.filterList(newText);
        return false;
    }

    public void setListAdapter(RecipeAdapter adapter) {
        searchFragment.setListAdapter(adapter);
    }
}
