package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.R;

/**
 * Created by brandon on 11/2/16.
 */

public class LoginFragment extends Fragment implements View.OnClickListener {
    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    private EditText emailInput;
    private EditText passwordInput;

    public interface AccountActionListener {
        void signIn(String email, String password, View view);

        void showSignUp();

        void showForgotPassword();
    }

    private AccountActionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AccountActionListener) {
            mListener = (AccountActionListener) context;
        } else {
            throw new IllegalArgumentException("AccountAction" +
                    "Listener interface not implemented.");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_account, menu);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailInput = ((EditText) view.findViewById(R.id.input_username));
        passwordInput = ((EditText) view.findViewById(R.id.input_password));
        view.findViewById(R.id.button_login).setOnClickListener(this);
        view.findViewById(R.id.button_signUp).setOnClickListener(this);
        view.findViewById(R.id.button_forgotPassword).setOnClickListener(this);
        view.findViewById(R.id.input_password).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    mListener.signIn(emailInput.getText().toString(), passwordInput.getText().toString(), getView());
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_info:
                Globals.accountInfoAlert(getActivity());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                if (Globals.genericFieldCheck(emailInput.getText().toString(), passwordInput.getText().toString(), getActivity())) {
                    return;
                }
                mListener.signIn(emailInput.getText().toString(), passwordInput.getText().toString(), getView());
                break;
            case R.id.button_signUp:
                mListener.showSignUp();
                break;
            case R.id.button_forgotPassword:
                mListener.showForgotPassword();
                break;
        }
    }
}
