package co.brandonr.fullsail.adp2recipeproject;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class StorageUtils {

    public static void writeIngredient(Context context, Ingredient ingredient, String filename) {
        File externalDir = context.getExternalFilesDir(null);
        File doc = new File(externalDir, filename);

        ArrayList<Ingredient> ingredients = readIngredients(context, filename);
        ingredients.add(ingredient);

        try {
            FileOutputStream fos = new FileOutputStream(doc);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(ingredients);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Ingredient> readIngredients(Context context, String filename) {

        ArrayList<Ingredient> data = new ArrayList<>();
        File externalDir = context.getExternalFilesDir(null);
        File doc = new File(externalDir, filename);

        try {
            FileInputStream fis = new FileInputStream(doc);
            ObjectInputStream ois = new ObjectInputStream(fis);
            data = ((ArrayList<Ingredient>) ois.readObject());
            ois.close();
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public static void deleteIngredient(Context context, Ingredient ingredient, String filename) {
        File externalDir = context.getExternalFilesDir(null);
        File doc = new File(externalDir, filename);
        ArrayList<Ingredient> storedData = readIngredients(context, filename);

        try {

            for (int i = 0; i < storedData.size(); i++) {
                Ingredient storedIngredient = storedData.get(i);
                if (ingredient.getmName().equals(storedIngredient.getmName())) {
                    storedData.remove(i);
                }
            }

            FileOutputStream fos = new FileOutputStream(doc);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(storedData);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateIngredient(Context context, Ingredient ingredient, double quantity, String filename) {
        File externalDir = context.getExternalFilesDir(null);
        File doc = new File(externalDir, filename);
        ArrayList<Ingredient> storedData = readIngredients(context, filename);

        try {

            for (int i = 0; i < storedData.size(); i++) {
                Ingredient storedIngredient = storedData.get(i);
                if (ingredient.getmName().equals(storedIngredient.getmName())) {
                    storedData.set(i, new Ingredient(ingredient.getmName(), quantity));
                }
            }

            FileOutputStream fos = new FileOutputStream(doc);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(storedData);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeFavorite(Context context, ActualRecipe recipe) {
        File externalDir = context.getExternalFilesDir(null);
        File doc = new File(externalDir, Globals.FAVORITE_DATA);

        ArrayList<ActualRecipe> favorites = readFavorites(context);
        favorites.add(recipe);

        try {
            FileOutputStream fos = new FileOutputStream(doc);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(favorites);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<ActualRecipe> readFavorites(Context context) {

        ArrayList<ActualRecipe> data = new ArrayList<>();
        File externalDir = context.getExternalFilesDir(null);
        File doc = new File(externalDir, Globals.FAVORITE_DATA);

        try {
            FileInputStream fis = new FileInputStream(doc);
            ObjectInputStream ois = new ObjectInputStream(fis);
            data = ((ArrayList<ActualRecipe>) ois.readObject());
            ois.close();
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public static void deleteFavorite(Context context, ActualRecipe recipe) {
        File externalDir = context.getExternalFilesDir(null);
        File doc = new File(externalDir, Globals.FAVORITE_DATA);
        ArrayList<ActualRecipe> storedData = readFavorites(context);

        try {

            for (int i = 0; i < storedData.size(); i++) {
                ActualRecipe storedRecipe = storedData.get(i);
                if (recipe.getTitle().equals(storedRecipe.getTitle())) {
                    storedData.remove(i);
                }
            }

            FileOutputStream fos = new FileOutputStream(doc);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(storedData);
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
