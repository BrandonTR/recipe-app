package co.brandonr.fullsail.adp2recipeproject.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import co.brandonr.fullsail.adp2recipeproject.ActualRecipe;
import co.brandonr.fullsail.adp2recipeproject.GetRecipeTask;
import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.Ingredient;
import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.Recipe;
import co.brandonr.fullsail.adp2recipeproject.StorageUtils;

public class RecipeActivity extends AppCompatActivity implements GetRecipeTask.TaskCompletedListener,
        View.OnClickListener {

    private Recipe passedRecipe;
    private ActualRecipe mRecipe;
    private ArrayList<Ingredient> missingIngredients;
    // private ArrayList<Ingredient> usedIngredients; Couldn't finish implementation :(

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        passedRecipe = (Recipe) getIntent().getSerializableExtra(Globals.RECIPE_EXTRA);
        getRecipe(passedRecipe);
    }

    @Override
    public void setViews(ActualRecipe recipe) {
        final ImageView imageView = (ImageView) findViewById(R.id.activity_recipe_image);
        TextView title = (TextView) findViewById(R.id.activity_recipe_title);
        TextView publisher = (TextView) findViewById(R.id.activity_recipe_publisher);
        TextView ingredients = (TextView) findViewById(R.id.ingredients);
        TextView missingView = (TextView) findViewById(R.id.missing_ingredients);
        TextView directions = (TextView) findViewById(R.id.directions);
        Button sourceButton = (Button) findViewById(R.id.button_source);
        if (recipe != null) {
            Picasso.with(this).load(recipe.getImageUrl()).into(imageView, null);
            mRecipe = recipe;
            title.setText(recipe.getTitle());
            publisher.setText(recipe.getPublisher());
            ingredients.setText(getString(R.string.ingredients) + "\n" + recipe.getIngredientString());
            directions.setText(recipe.getInstructions());
            sourceButton.setOnClickListener(this);
            missingIngredients = passedRecipe.getMissingArray();
            missingView.setText("Missing Ingredients" + "\n" + passedRecipe.getMissingString());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_source:
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(mRecipe.getSourceUrl()));
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_recipe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_complete:
//                for (Ingredient ingredient : usedIngredients) {
//                    StorageUtils.deleteIngredient(this, ingredient, Globals.INGREDIENTS_DATA);
//                }
                finish();
                break;
            case R.id.action_favorite:
                StorageUtils.writeFavorite(this, mRecipe);
                break;
            case R.id.action_addToShopping:
                for (Ingredient ingredient : missingIngredients) {
                    StorageUtils.writeIngredient(this, ingredient, Globals.SHOPPING_DATA);
                }
        }
        return super.onOptionsItemSelected(item);
    }

    private void getRecipe(Recipe recipe) {
        try {
            checkAndExecute(Globals.getRecipeUrl(recipe.getRecipeId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkAndExecute(String query) {
        GetRecipeTask getRecipesTask = new GetRecipeTask(this);

        if (Globals.connected(this)) {
            getRecipesTask.execute(query);
        } else {
            Globals.noInternetToast(this);
        }
    }
}
