package co.brandonr.fullsail.adp2recipeproject.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.fragments.FavoritesFragment;
import co.brandonr.fullsail.adp2recipeproject.fragments.PantryFragment;
import co.brandonr.fullsail.adp2recipeproject.fragments.ShoppingFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                PantryFragment.newInstance()).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        PantryFragment.newInstance()).commit();
                break;
            case R.id.action_shopping:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        ShoppingFragment.newInstance()).commit();
                break;
            case R.id.action_favorites:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        FavoritesFragment.newInstance()).commit();
                break;
        }
        return false;
    }
}
