package co.brandonr.fullsail.adp2recipeproject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by brandon on 11/10/16.
 */

public class GetRecipeTask extends AsyncTask<String, Void, String> {

    public interface TaskCompletedListener {
        void setViews(ActualRecipe recipe);
    }

    private TaskCompletedListener mListener;
    private ProgressDialog progressDialog;
    private final Context context;

    public GetRecipeTask(Context context) {
        this.context = context;
        if (context instanceof TaskCompletedListener) {
            mListener = (TaskCompletedListener) context;
        } else {
            throw new IllegalArgumentException("TaskCompleted" +
                    "Listener interface not implemented.");
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Working");
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            URL url = new URL(strings[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("X-Mashape-Key", "5eWoDTc8EImsh64IdaqPAoDhF6P8p1Y90LijsnEQwWtPjNA5db");
            connection.setRequestProperty("Accept", "application/json");

            try {
                connection.connect();

                InputStream is = connection.getInputStream();

                String data = IOUtils.toString(is);

                is.close();

                return data;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // Close connection
                connection.disconnect();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "Error";
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        progressDialog.cancel();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        JSONObject recipeObject;

        try {
            recipeObject = new JSONObject(s);
            ActualRecipe actualRecipe = new ActualRecipe(recipeObject.getString("image"),
                    getIngredientString(recipeObject.getJSONArray("extendedIngredients")),
                    getIngredients(recipeObject.getJSONArray("extendedIngredients")),
                    recipeObject.getString("sourceName"), recipeObject.getString("sourceUrl"),
                    recipeObject.getString("title"), recipeObject.getString("instructions"));
            mListener.setViews(actualRecipe);
            progressDialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getIngredientString(JSONArray ingredients) {
        String ingredientsString = "";
        for (int i = 0; i < ingredients.length(); i++) {
            try {
                JSONObject ingredient = ingredients.getJSONObject(i);
                ingredientsString = ingredientsString.concat("• " + ingredient.getString("originalString")
                        + "\n");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ingredientsString;
    }

    private ArrayList<Ingredient> getIngredients(JSONArray ingredients) {
        ArrayList<Ingredient> ingredientArray = new ArrayList<>();
        for (int i = 0; i < ingredients.length(); i++) {
            try {
                JSONObject ingredient = ingredients.getJSONObject(i);
                ingredientArray.add(new Ingredient(WordUtils.capitalize(ingredient.getString("name")),
                        ingredient.getInt("amount")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ingredientArray;
    }
}