package co.brandonr.fullsail.adp2recipeproject.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import co.brandonr.fullsail.adp2recipeproject.ActualRecipe;
import co.brandonr.fullsail.adp2recipeproject.Globals;
import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.StorageUtils;

/**
 * Created by brandon on 11/17/16.
 */

public class FavoriteFragment extends Fragment implements View.OnClickListener {
    public static FavoriteFragment newInstance() {
        return new FavoriteFragment();
    }

    private ActualRecipe recipe;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        recipe = (ActualRecipe) getActivity().getIntent().getSerializableExtra(Globals.FAVORITE_EXTRA);
        return inflater.inflate(R.layout.fragment_favorite, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ImageView imageView = (ImageView) view.findViewById(R.id.favorite_image);
        TextView title = (TextView) view.findViewById(R.id.favorite_title);
        TextView publisher = (TextView) view.findViewById(R.id.favorite_publisher);
        TextView ingredients = (TextView) view.findViewById(R.id.favorite_ingredients);
        TextView directions = (TextView) view.findViewById(R.id.favorite_directions);
        Button sourceButton = (Button) view.findViewById(R.id.button_source);
        if (recipe != null) {
            Picasso.with(getActivity()).load(recipe.getImageUrl()).into(imageView, null);
            title.setText(recipe.getTitle());
            publisher.setText(recipe.getPublisher());
            ingredients.setText(getString(R.string.ingredients) + "\n" + recipe.getIngredientString());
            directions.setText(recipe.getInstructions());
            sourceButton.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_source:
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(recipe.getSourceUrl()));
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_favorite, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                StorageUtils.deleteFavorite(getActivity(), recipe);
                getActivity().finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
