package co.brandonr.fullsail.adp2recipeproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.commons.lang.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by brandon on 11/10/16.
 */

public class RecipeAdapter extends BaseAdapter implements Filterable {

    private final Context context;
    private JSONArray recipes;
    private JSONArray mOriginalValues;

    public RecipeAdapter(Context context, JSONArray recipes) {
        this.context = context;
        this.recipes = recipes;
    }

    @Override
    public int getCount() {
        if (recipes != null) {
            return recipes.length();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int i) {
        if (recipes != null && i < recipes.length() && i >= 0) {
            try {
                JSONObject recipe = recipes.getJSONObject(i);
                JSONArray missingIngredients = recipe.getJSONArray("missedIngredients");
                return new Recipe(recipe.getString("title"), recipe.getString("image"), recipe.getInt("id"),
                        recipe.getInt("usedIngredientCount"), missingIngredients.length(),
                        recipe.getInt("likes"), getIngredients(missingIngredients), getIngredientString(missingIngredients));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private String getIngredientString(JSONArray ingredients) {
        String ingredientsString = "";
        if (ingredients.length() == 0) {
            ingredientsString = ingredientsString.concat("• " + "No missing ingredients!");
        }
        for (int i = 0; i < ingredients.length(); i++) {
            try {
                JSONObject ingredient = ingredients.getJSONObject(i);
                ingredientsString = ingredientsString.concat("• " + ingredient.getString("originalString")
                        + "\n");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ingredientsString;
    }

    private ArrayList<Ingredient> getIngredients(JSONArray ingredients) {
        ArrayList<Ingredient> ingredientArray = new ArrayList<>();
        for (int i = 0; i < ingredients.length(); i++) {
            try {
                JSONObject ingredient = ingredients.getJSONObject(i);
                ingredientArray.add(new Ingredient(WordUtils.capitalize(ingredient.getString("name")),
                        ingredient.getInt("amount")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ingredientArray;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.cell_recipe, viewGroup, false);
        }

        Recipe recipe = (Recipe) getItem(i);

        final ImageView imageView = (ImageView) view.findViewById(R.id.recipe_image);
        TextView title = (TextView) view.findViewById(R.id.recipe_title);
        TextView likes = (TextView) view.findViewById(R.id.recipe_likes);
        TextView used = (TextView) view.findViewById(R.id.recipe_used);
        TextView missing = (TextView) view.findViewById(R.id.recipe_missing);

        if (recipe != null) {
            Picasso.with(context).load(recipe.getImageUrl()).into(imageView, null);
            title.setText(recipe.getTitle());
            likes.setText("Likes: " + recipe.getLikes());
            used.setText("Used: " + recipe.getUsedIngredients());
            missing.setText("Missing: " + recipe.getMissingIngredients());
        }

        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                recipes = (JSONArray) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                JSONArray FilteredArrList = new JSONArray();

                if (mOriginalValues == null) {
                    mOriginalValues = recipes;
                }

                if (constraint == null || constraint.length() == 0) {
                    results.count = mOriginalValues.length();
                    results.values = mOriginalValues;
                } else {
                    for (int i = 0; i < mOriginalValues.length(); i++) {
                        JSONObject data;
                        try {
                            data = mOriginalValues.getJSONObject(i);
                            String test = data.getString("title");
                            if (test.toLowerCase(Locale.getDefault()).contains(constraint)) {
                                FilteredArrList.put(data);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    results.count = FilteredArrList.length();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
    }
}
