package co.brandonr.fullsail.adp2recipeproject.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.brandonr.fullsail.adp2recipeproject.R;
import co.brandonr.fullsail.adp2recipeproject.fragments.FavoriteFragment;

public class FavoriteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        getSupportFragmentManager().beginTransaction().add(
                R.id.favorite_frame, FavoriteFragment.newInstance()).commit();
    }
}
